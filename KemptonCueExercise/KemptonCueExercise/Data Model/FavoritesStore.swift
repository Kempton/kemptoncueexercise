//
//  FavoritesStore.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation

final class FavoriteStore {
    
    static let sharedInstance = FavoriteStore()
    var favoriteResults = [Result]()
    private var filePath: String!
    private var fileURL: URL!
    private let manager = FileManager.default
    
    // MARK: Store Presistent Data
    
    init() {
        let documents = manager.urls(for: .documentDirectory, in: .userDomainMask)
        let docURL = documents.first!
        //to see this is finder, cd ~/, cd ../../, cd Users into docURL path printed below, open .
        //print("docURL")
        fileURL = docURL.appendingPathComponent("favorites.dat")
        filePath = fileURL.path
        if manager.fileExists(atPath: filePath) {
            guard let content = manager.contents(atPath: filePath),
                  let data = try? NSKeyedUnarchiver.unarchivedObject(ofClass: NSData.self, from: content) as Data?,
                  let results = try? PropertyListDecoder().decode([Result].self, from: data) else {
                    return
            }
                favoriteResults = results
                //print("file exists = \(favoriteResults), count = \(favoriteResults.count)")
                //print("current favs = \(favoriteResults)")
        } else {
            guard let data = try? PropertyListEncoder().encode(favoriteResults),
                  let fileData = try? NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false) else { return }
            manager.createFile(atPath: filePath, contents: fileData, attributes: nil)
            //print("file does not exist creating new store")
            //print("current favs = \(favoriteResults)")
        }
    }

    func storeResult() {
        guard let data = try? PropertyListEncoder().encode(favoriteResults),
              let fileData = try? NSKeyedArchiver.archivedData(withRootObject: data, requiringSecureCoding: false) else { return }
        do {
            try fileData.write(to: fileURL)
            //print("Success, wrote to file")
            //print("current favs = \(favoriteResults)")
        } catch {
            //print("Failure, couldn't write to file")
        }
    }
}
