//
//  Networking.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import Foundation
import Combine

enum HTTPError: LocalizedError {
    case statusCode
}

final class Networking: NSObject {
    static let sharedInstance = Networking()
    static let defaultPath = "https://api.themoviedb.org/3/movie/now_playing?api_key=f4373e06757e4b7d91999d0c0f5a63e8&language=en-US"
    private let manager = FileManager.default
    private var cancellable: AnyCancellable?
    
    func getNowPlayingList(_ url: String, completionHandler: @escaping (Results) -> Void) {
        guard let url = URL(string: url) else { return }
        NotificationCenter.default.post(name: .ShowLoadingSpinner, object: nil)
        
        let urlRequest = configureURLRequest(url)
        
        cancellable = URLSession.shared.dataTaskPublisher(for: urlRequest)
            .tryMap { output in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    //will fall here with wrong url
                    self.displayErrorAlertView()
                    throw HTTPError.statusCode
                }
                return output.data
        }
        .decode(type: Results.self, decoder: JSONDecoder())
        .eraseToAnyPublisher()
        .sink(receiveCompletion: { completion in
            switch completion {
            case .finished:
                break
            case .failure(let error):
                //will fall here with no internet
                self.displayErrorAlertView()
                print(error)
            }
        }, receiveValue: { results in
//            print("receive value \(results.results)")
            completionHandler(results)
            self.hideLoadingSpinner()
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(results) {
                //should save to disc at some point, not gotten around to this yet
                //self.saveNowPlayingToDisc(encoded)
            }
        })
    }
    
    // MARK: Private
    
    private func configureURLRequest(_ url: URL) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        urlRequest.httpMethod = "GET"
        return urlRequest
    }
    
    private func displayErrorAlertView() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .DisplayErrorAlertView, object: nil)
        }
    }
    
    private func hideLoadingSpinner() {
        DispatchQueue.main.async {
            NotificationCenter.default.post(name: .HideLoadingSpinner, object: nil)
        }
    }
}

