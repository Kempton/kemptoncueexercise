//
//  FavoritesViewController.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {

    private var nowPlayingCollectionViewController: NowPlayingCollectionViewController?
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nowPlayingCollectionViewController?.results = FavoriteStore.sharedInstance.favoriteResults
    }
    
    override func viewWillAppear(_ animated: Bool) {
        nowPlayingCollectionViewController?.results = FavoriteStore.sharedInstance.favoriteResults
        nowPlayingCollectionViewController?.collectionView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "favoritesList" {
            let nowPlayingCollectionViewController = segue.destination as! NowPlayingCollectionViewController
            self.nowPlayingCollectionViewController = nowPlayingCollectionViewController
        }
    }

}
