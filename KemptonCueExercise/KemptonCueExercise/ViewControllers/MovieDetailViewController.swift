//
//  MovieDetailViewController.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit
import SDWebImage

class MovieDetailViewController: UIViewController {

    var movieTitle: String?
    var movieDescription: String?
    var movieReleaseDate: String?
    var posterPath: String?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
    // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let staticTextAttributes = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17)]
        
        //using if lets because some might come back and others not
        if let movieTitle = movieTitle {
            let attributedTitle = NSMutableAttributedString(string: movieTitle)
            let staticTitleText = NSMutableAttributedString(string: "Title: ", attributes: staticTextAttributes)
            let title = NSMutableAttributedString()
            title.append(staticTitleText)
            title.append(attributedTitle)
            titleLabel.attributedText = title
            self.title = movieTitle
        } else {
            titleLabel.attributedText = NSAttributedString(string: "Missing Title")
        }
        
        if let movieDescription = movieDescription {
            let attributedDescription = NSMutableAttributedString(string: movieDescription)
            let staticDescriptionText = NSMutableAttributedString(string: "Description: ", attributes: staticTextAttributes)
            let description = NSMutableAttributedString()
            description.append(staticDescriptionText)
            description.append(attributedDescription)
            descriptionLabel.attributedText = description
        } else {
            descriptionLabel.attributedText = NSAttributedString(string: "Missing Description")
        }
        
        if let movieReleaseDate = movieReleaseDate {
            let attributedReleaseDate = NSMutableAttributedString(string: movieReleaseDate)
            let staticReleaseDateText = NSMutableAttributedString(string: "Release Date: ", attributes: staticTextAttributes)
            let releaseDate = NSMutableAttributedString()
            releaseDate.append(staticReleaseDateText)
            releaseDate.append(attributedReleaseDate)
            releaseDateLabel.attributedText = releaseDate
        } else {
            releaseDateLabel.attributedText = NSAttributedString(string: "Missing Release Date")
        }
        
        
        if let posterPath = posterPath {
            let posterUrl = URL(string: "https://image.tmdb.org/t/p/w500/" + posterPath)
            posterImageView?.sd_setImage(with: posterUrl,
                                         placeholderImage: nil,
                                         options: .retryFailed) { (image, error, type, url) in
                                            if error != nil {
                                                //if there is an error, timeout or whatnot add our missing image placeholder
                                                //note there is no placeholder in this project
                                                self.posterImageView.image = UIImage(named: "questionMark")
                                            }
            }
        }
    }
}
