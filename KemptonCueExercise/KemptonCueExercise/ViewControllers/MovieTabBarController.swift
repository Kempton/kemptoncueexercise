//
//  MovieTabBarController.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class MovieTabBarController: UITabBarController {
    private let activityIndicator = UIActivityIndicatorView()
    
     // MARK: View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(showLoadingSpinner), name: .ShowLoadingSpinner, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideLoadingSpinner), name: .HideLoadingSpinner, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(displayErrorAlertView), name: .DisplayErrorAlertView, object: nil)
                
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[activityIndicator(40)]",
                                                      options: [], metrics: nil, views: ["activityIndicator": activityIndicator]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[activityIndicator(40)]",
                                                      options: [], metrics: nil, views: ["activityIndicator": activityIndicator]))
        
        NSLayoutConstraint.activate([
             activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor)
             ])
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.25)
        activityIndicator.layer.cornerRadius = 10
    }
    
    // MARK: Notifications
    
    @objc func showLoadingSpinner() {
        activityIndicator.startAnimating()
    }
    
    @objc func hideLoadingSpinner() {
        activityIndicator.stopAnimating()
    }
    
    @objc func displayErrorAlertView() {
        activityIndicator.stopAnimating()
        let alertController = UIAlertController(title: "Whoops", message: "Something went wrong fetching your data", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Try Again", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
}

extension Notification.Name {
    static let ShowLoadingSpinner = NSNotification.Name("ShowLoadingSpinner")
    static let HideLoadingSpinner = NSNotification.Name("HideLoadingSpinner")
    static let DisplayErrorAlertView = NSNotification.Name("DisplayErrorAlertView")
}
