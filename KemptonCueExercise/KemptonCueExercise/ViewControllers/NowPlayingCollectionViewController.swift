//
//  NowPlayingCollectionViewController.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit
import SDWebImage

protocol NowPlayingCollectionViewControllerDelegate: class {
    func loadMoreIfPossible()
}

private let reuseIdentifier = "Cell"

class NowPlayingCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, SaveToFavoritesDelegate {

    let margin = CGFloat(10)
    var loadMore = true
    weak var delegate: NowPlayingCollectionViewControllerDelegate?
    
    var results: [Result]? {
        didSet {
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumLineSpacing = margin
        flowLayout.minimumInteritemSpacing = margin
        collectionView?.collectionViewLayout = flowLayout

        clearsSelectionOnViewWillAppear = false
    }

    // MARK: Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "movieDetail" {
            if let indexPaths = collectionView.indexPathsForSelectedItems, let results = results {
                let destinationController = segue.destination as! MovieDetailViewController
                let index = indexPaths[0]
                destinationController.movieTitle = results[index.row].originalTitle
                destinationController.posterPath = results[index.row].posterPath
                destinationController.movieDescription = results[index.row].overview
                destinationController.movieReleaseDate = results[index.row].releaseDate
            }
        }
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return results?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! MovieCollectionViewCell
        guard let movie = results?[indexPath.row] else { return UICollectionViewCell() }
        cell.titleLabel.text = movie.originalTitle
        cell.id = movie.id
        cell.delegate = self
        cell.setFavoriteState(false)
        
        for favoriteMovie in FavoriteStore.sharedInstance.favoriteResults {
            if favoriteMovie.id == movie.id {
                cell.setFavoriteState(true)
            }
        }
        
        if let posterPath = movie.posterPath {
            let posterUrl = URL(string: "https://image.tmdb.org/t/p/w300/" + posterPath)
            cell.posterImageView?.sd_setImage(with: posterUrl,
                                              placeholderImage: nil,
                                              options: .retryFailed) { (image, error, type, url) in
                                                if error != nil {
                                                    //if there is an error, timeout or whatnot add our missing image placeholder
                                                    //note there is no placeholder in this project
                                                    cell.posterImageView.image = nil
                                                }
            }
        } else {
            cell.posterImageView.image = UIImage(named: "questionMark")
        }
        
        cell.backgroundColor = UIColor.lightGray
        
        if let results = results, loadMore {
            if indexPath.row == results.count - 1 {
                delegate?.loadMoreIfPossible()
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.safeAreaLayoutGuide.layoutFrame.width / 2 - 5
        return CGSize(width: (view.safeAreaLayoutGuide.layoutFrame.width / 2 - 5), height: width + 100)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }

    //MARK: SaveToFavoritesDelegate
    
    func saveToFavorites(id: Int) {
        //if the result already exists in the favorite store remove it and update the store
        //the id acts as a UUID here
        for storedResult in FavoriteStore.sharedInstance.favoriteResults {
            if storedResult.id == id {
                FavoriteStore.sharedInstance.favoriteResults = FavoriteStore.sharedInstance.favoriteResults.filter { $0 != storedResult }
                FavoriteStore.sharedInstance.storeResult()
                return
            }
        }
        
        if let results = self.results {
            //find the result we want to add to our store and save it
            for result in results {
                if result.id == id {
                    //save to the store
                    FavoriteStore.sharedInstance.favoriteResults.append(result)
                    FavoriteStore.sharedInstance.storeResult()
                }
            }
        }
        
    }
}
