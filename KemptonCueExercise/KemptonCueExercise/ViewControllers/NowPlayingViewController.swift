//
//  NowPlayingViewController.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

class NowPlayingViewController: UIViewController, NowPlayingCollectionViewControllerDelegate {
    
    private var nowPlayingCollectionViewController: NowPlayingCollectionViewController?
    private var currentSearchPage = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Networking.sharedInstance.getNowPlayingList(Networking.defaultPath) { [weak self] (results) in
            guard let nowPlayingCollectionViewController = self?.nowPlayingCollectionViewController else { return }
            nowPlayingCollectionViewController.results = results.results
        }
    }
    
    // MARK: Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "nowPlayingList" {
            let nowPlayingCollectionViewController = segue.destination as! NowPlayingCollectionViewController
            self.nowPlayingCollectionViewController = nowPlayingCollectionViewController
            self.nowPlayingCollectionViewController?.delegate = self
        }
    }
    
    //MARK: NowPlayingCollectionViewControllerDelegate
    
    func loadMoreIfPossible() {
        currentSearchPage = currentSearchPage + 1
        let url = Networking.defaultPath + "&page=" + String(currentSearchPage)
        Networking.sharedInstance.getNowPlayingList(url) { [weak self] (results) in
            guard let nowPlayingCollectionViewController = self?.nowPlayingCollectionViewController,
                  let previousResults = nowPlayingCollectionViewController.results else { return }
            //is the API returns an empty array we know there are no more pages, tell the collectionview to stop trying to load more
            if results.results.isEmpty {
                self?.nowPlayingCollectionViewController?.loadMore = false
                return
            }
            //append the next page to the current book list
            let newResultsList = previousResults + results.results
            nowPlayingCollectionViewController.results = newResultsList
        }
    }
}

