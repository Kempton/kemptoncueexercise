//
//  MovieCollectionViewCell.swift
//  KemptonCueExercise
//
//  Created by Justin Kempton on 2/23/20.
//  Copyright © 2020 None. All rights reserved.
//

import UIKit

protocol SaveToFavoritesDelegate: class {
    func saveToFavorites(id: Int)
}

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var heartButton: UIButton!
    
    var id: Int?
    var isFavorite = false
    
    weak var delegate: SaveToFavoritesDelegate?
    
    override func prepareForReuse() {
        titleLabel.text = ""
        posterImageView.image = nil
        setFavoriteState(false)
    }
    
    @IBAction func save(_ sender: UIButton) {
        guard let id = id else { return }
        setFavoriteState(!isFavorite)
        delegate?.saveToFavorites(id: id)
        sender.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {
                        sender.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
    }
    
    // MARK: Favoriting
    
    func setFavoriteState(_ value: Bool) {
        isFavorite = value
        let determinedHeartIcon = value ? UIImage(named: "heartFilledIcon") : UIImage(named: "heartIcon")
        let heartIcon = determinedHeartIcon?.withRenderingMode(.alwaysTemplate)
        heartButton.setImage(heartIcon, for: .normal)
        heartButton.tintColor = UIColor.red
    }
}
